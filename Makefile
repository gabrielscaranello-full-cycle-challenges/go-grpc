# Variables
PROTOC_GEN_GO_VERSION = v1.28
PROTOC_GEN_GO_GRPC_VERSION = v1.2

proto-install:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@${PROTOC_GEN_GO_VERSION}
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@${PROTOC_GEN_GO_GRPC_VERSION}

proto-run:
	protoc --go_out=. --go-grpc_out=. proto/*
