
# Full Cycle Course - Go gRPC

This repository is part of a series of codes created to meet the challenges of the Full Cycle 3.0 course

## Running

```sh
go run ./cmd/grpc_server/main.go
```
