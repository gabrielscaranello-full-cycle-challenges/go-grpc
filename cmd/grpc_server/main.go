package main

import (
	"database/sql"
	"log"
	"net"

	"gitlab.com/gabrielscaranello-full-cycle-challenges/go-grpc/internal/database"
	"gitlab.com/gabrielscaranello-full-cycle-challenges/go-grpc/internal/pb"
	"gitlab.com/gabrielscaranello-full-cycle-challenges/go-grpc/internal/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	_ "github.com/mattn/go-sqlite3"
)

func createTables(db *sql.DB) {
	sql := `CREATE TABLE IF NOT EXISTS categories (
		id TEXT PRIMARY KEY,
		name TEXT NOT NULL,
		description TEXT
	);`

	_, err := db.Exec(sql)
	if err != nil {
		log.Fatalf("failed to create tables: %v", err)
	}
}

func main() {
	db, err := sql.Open("sqlite3", "./data.db")
	if err != nil {
		log.Fatalf("failed to open database: %v", err)
	}
	defer db.Close()

	createTables(db)

	categoryDB := database.NewCategory(db)

	categoryService := service.NewCategoryService(*categoryDB)

	grpcServer := grpc.NewServer()

	pb.RegisterCategoryServiceServer(grpcServer, categoryService)

	reflection.Register(grpcServer)

	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
